# POKEMONAPI

Projeto Desafio
Testes de serviço para validar requests da API Pokemon.

Requisitos


node v12.18.3
npm v6.14.6


Instalando dependências

npm install -g newman


Executando em linha de comando
newman run Pokemon.postman_collection.json --environment postman_environment.json

Executando com Docker
docker build -t postman-test .
docker run postman-test

